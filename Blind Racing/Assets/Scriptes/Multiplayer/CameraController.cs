﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraController : MonoBehaviour {

    private Transform target;
    private Camera cam;

    private void Start()
    {
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

    public GameObject cameraPrefab;
    public GameObject playerPrefab;

    private GameObject myPlayer;
    private GameObject cameraObject;
    private GameObject networkManager;

    void Start () {
        // Is this my own local Player
        if (!isLocalPlayer)
        {
            return;
        }
        //networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
        CmdSpawnMyUnit();
        //networkManager.GetComponent<WaitForPlayers>().CmdIncreaseNum();
    }

    public override void OnStartLocalPlayer()
    {

        //Camera.main.GetComponent<CameraController>().SetTarget(gameObject.transform);
    }

    public GameObject GetMyPlayer()
    {
        return myPlayer;
    }

    [Command]
    public void CmdSpawnMyUnit()
    {
        GameObject playerObject = Instantiate(playerPrefab, gameObject.transform.position, Quaternion.identity, null);
        myPlayer = playerObject;

        GameObject.FindGameObjectWithTag("NetworkManager").GetComponent<WaitForPlayers>().CmdIncreaseNum();
        NetworkServer.SpawnWithClientAuthority(playerObject, connectionToClient);
    }
    

}

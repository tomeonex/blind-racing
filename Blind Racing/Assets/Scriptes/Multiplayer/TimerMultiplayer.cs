﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerMultiplayer : MonoBehaviour
{
    
    private float timer = -5f;
    private float lastLapTime;

    private CarMultiplayer car;

    void Start()
    {
        car = GetComponentInParent<CarMultiplayer>();
    }

    void Update()
    {
        
        timer += Time.deltaTime;
        if (timer < 0f)
        {
            return;
        }

        car.SetRdy(true);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.transform.tag == "Car")
        {
            SaveLapTime();
            timer = 0f;
        }
    }

    public void SaveLapTime()
    {
        lastLapTime = timer;
        if (lastLapTime > PlayerPrefs.GetFloat("FastestLap", 0))
        {
            PlayerPrefs.SetFloat("FastestLap", lastLapTime);
        }
    }
}

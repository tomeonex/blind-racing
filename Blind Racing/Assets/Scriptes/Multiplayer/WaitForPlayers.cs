﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WaitForPlayers : NetworkBehaviour {

    [SyncVar]
    private int playersNum = 0;

	void Start () {

        Time.timeScale = 0f;
	}
	
	void Update () {

        Debug.Log(playersNum + " " + Time.timeScale);
		if(playersNum == 2)
        {
            Time.timeScale = 1f;
        }
	}
    
    public int GetPlayerNum()
    {
        return playersNum;
    }

    [Command]
    public void CmdIncreaseNum()
    {

        playersNum++;
        Debug.Log("elo" + playersNum);
    }

}

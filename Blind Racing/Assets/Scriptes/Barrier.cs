﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour {

    public Collider2D barrier;
    public Collider2D enableCollider;
    public Collider2D disableCollider;

    private Collider2D meta;

    
	void Start () {
        meta = GetComponent<Collider2D>();
        meta.enabled = false;
        barrier.enabled = false;
	}
	
    public void EnableBarrier()
    {
        barrier.enabled = true;
        meta.enabled = true;
    }

    public void DisableBarrier()
    {
        barrier.enabled = false;
    }
}

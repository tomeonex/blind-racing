﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableBarrier : MonoBehaviour {

    public void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponentInParent<Barrier>().EnableBarrier();
    }
}

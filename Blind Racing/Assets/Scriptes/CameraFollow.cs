﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;

    private AudioManager audioManager;

    private void Awake()
    {
        audioManager = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
            transform.position = new Vector3(target.position.x, target.position.y, -10f);
    }

    public void Sounds()
    {
        audioManager.ChangeVolume();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text countdown;
    public Text time;
    public Text lastLapText;
    
    private float timer = -5f;
    private float lastLapTime;

    private CarController car;

	void Start () {
        car = FindObjectOfType<CarController>();
	}
	
	void Update () {

        
        timer += Time.deltaTime;
        if (timer < 0f)
        {
            countdown.text = timer.ToString("0.00");
            return;
        }
        countdown.text = "";
        car.SetRdy(true);
        
        time.text = timer.ToString("0.00");

        lastLapText.text = "Last lap time: " + lastLapTime.ToString("0.00");
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.transform.tag == "Car")
        {
            SaveLapTime();
            timer = 0f;
        }
    }

    public void SaveLapTime()
    {
        lastLapTime = timer;
        if(lastLapTime > PlayerPrefs.GetFloat("FastestLap", 0))
        {
            PlayerPrefs.SetFloat("FastestLap", lastLapTime);
        }
    }
}

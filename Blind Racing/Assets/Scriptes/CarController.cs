﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

    public float speed;
    public float torque;
    public float driftSticky;
    public float driftSlippy;
    public float maxStickyVelocity;
    public float minSlippyVelocity;

    public TrailRenderer[] trails;

    private float tempSpeed;
    private float drift;
    private bool isSlippy = false;
    private bool isAudio = false;
    private bool rdy = false;

    private AudioManager audioManager;
    private Rigidbody2D rig;

	void Start () {

        tempSpeed = speed;

        audioManager = FindObjectOfType<AudioManager>();

        rig = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {

        if (isSlippy && isAudio)
        {
            EnableTrails();
        }
        else DisableTrails();
    }
    void FixedUpdate () {

        if (!rdy) return;

        if (isSlippy) drift = driftSlippy;
        else drift = driftSticky;

        if(RightdVelocity().magnitude > maxStickyVelocity)
        {
            drift = driftSlippy;
            isSlippy = true;
            isAudio = true;
        }
        if(RightdVelocity().magnitude < minSlippyVelocity)
        {
            isSlippy = false;
            isAudio = false;
        }

        rig.velocity = ForwardVelocity() + RightdVelocity() * drift;

        if (Input.GetButton("Accelerate"))
        {
            rig.AddForce(transform.up * tempSpeed);
        }

        if (Input.GetButton("Brakes"))
        {
            rig.AddForce(transform.up * -tempSpeed/2);
        }

        rig.angularVelocity = Input.GetAxis("Horizontal") * Mathf.Lerp(0, torque, rig.velocity.magnitude / 10);
        
	}

    public Vector2 ForwardVelocity()
    {
        return transform.up * Vector2.Dot(rig.velocity, transform.up);
    }

    public Vector2 RightdVelocity()
    {
        return transform.right * Vector2.Dot(rig.velocity, transform.right);
    }

    public void EnableTrails()
    {
        foreach(TrailRenderer t in trails)
        {
            t.emitting = true;
        }

        if (!audioManager.IsPlaying("Drift"))
        {
            audioManager.Play("Drift");
        }
    }

    public void DisableTrails()
    {
        foreach (TrailRenderer t in trails)
        {
            t.emitting = false;
        }

        if (audioManager.IsPlaying("Drift"))
        {
            audioManager.Stop("Drift");
        }
    }
    
        public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Racecourse")
        {
            tempSpeed = speed / 2f;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Racecourse")
        {
            tempSpeed = speed;
        }
    }

    public void SetRdy(bool value)
    {
        rdy = value;
    }
}
